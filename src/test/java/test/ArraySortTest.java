package test;
import static org.junit.Assert.*;

import org.junit.Test;

import test1.ArraySort;


public class ArraySortTest {

	ArraySort arraySort = new ArraySort();
	
	//input -> {10, 3, 67, 2} output -> {2, 3,10,67}
	@Test
	public final void testArraySort_ASC() {
		int input[] = {10,3,67,2};
		assertArrayEquals(new int[] {2,3,10,67}, arraySort.arraySortAsc(input));	
	}
	
	//to handle the null pointer exception
	@Test(expected=NullPointerException.class)
	public final void testArraySort_NullArray() {
		int input[] = null;
		arraySort.arraySortAsc(input);
	}
	
	//empty Array
	@Test
	public final void testArraySort_emptyArray() {
		int input[] = {};
		assertArrayEquals(new int[] {}, arraySort.arraySortAsc(input));	
	}

	//for 50 mili seconds
	@Test(timeout=50)
	public final void testArraySort_Performance() {
		int input[] = {10,3,67,2};
		for (int i=0;i<1000000;i++) {
			input[0]=i;
			arraySort.arraySortAsc(input);
		}
	}
}
